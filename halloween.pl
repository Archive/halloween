#!/usr/bin/perl -w

use strict;

use Halloween;
use Carp;

die "Usage: $0 spooldir id [id ...]" unless $#ARGV >= 0;

my $h = new Halloween;

$h->spooldir (shift @ARGV);

my @output;

sub do_read($) {
    my ($id) = @_;

    my $bug = eval { $h->read_bugreport ($id) };
    foreach ($h->messages) {
	warn "$id: $_";
    }
    $h->clear_messages;

    return $bug;
}

my $f;
foreach $f (@ARGV) {
    my $bug = do_read ($f);
    next unless ref $bug;

    push @output, $h->output_bugreport ($f, $bug);
}

exit unless $#output >= 0;

print qq[<?xml version="1.0" ?>\n];
print qq[<!DOCTYPE halloween SYSTEM "http://bugzilla.gnome.org/bugzilla.gnome.org/bugzilla-gnome-org.dtd">\n];
print qq[<halloween>\n];
print join '', @output;
print qq[</halloween>\n];

