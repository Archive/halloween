#!/usr/local/perl/bin/perl -wT

use strict;

use Halloween;
use CGI;
use CGI::Carp qw(fatalsToBrowser);;

my $h = new Halloween;
my $q = new CGI;

$h->spooldir ('/opt/debbugs/spool/db');

my $id = $q->param('id');
die "Usage error" unless defined $id and $id =~ /^\d+$/;

my @output;

sub do_read($) {
    my ($id) = @_;

    my $bug = eval { $h->read_bugreport ($id) };
    foreach ($h->messages) {
	warn "$id: $_";
    }
    $h->clear_messages;

    return $bug;
}

my $bug = do_read ($id);
die "Can't read bug \#$id" unless ref $bug;
push @output, $h->output_bugreport ($id, $bug);

print $q->header(-type => 'text/plain');
print qq[<?xml version="1.0" ?>\n];
print qq[<!DOCTYPE halloween SYSTEM "http://bugzilla.gnome.org/bugzilla.gnome.org/bugzilla-gnome-org.dtd">\n];
print qq[<halloween>\n];
print join '', @output;
print qq[</halloween>\n];

