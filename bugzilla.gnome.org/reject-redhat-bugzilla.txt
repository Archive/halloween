Your bug report has been rejected by the GNOME Bugtracking system.

This bug is against a package provided by Red Hat, not by GNOME
itself. Red Hat check for bugs in their bugzilla first. Please
take a few minutes to create an account at
       http://bugzilla.redhat.com/bugzilla/
and enter it there. It will receive faster attention that way.

Thanks!


