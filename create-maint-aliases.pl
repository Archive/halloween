#!/usr/bin/perl -wT

use strict;

$ENV{PATH} = '/bin:/usr/bin';
$ENV{ENV} = '';

open MAINTALIASES, "/home/halloween/halloween/bugzilla.gnome.org/maint-aliases.txt" or
    die "open (maint-aliases.txt): $!";
while (<MAINTALIASES>) {
    chop;
    next if /^\#/;
    next if /^\s*$/;

    unless (/^([\w\d\-]+):\s*(.*?)\s*$/) {
	warn "Invalid line '$_'";
	next;
    }

    print qq[$1-maint\@bugzilla.gnome.org $2\n];
}
close MAINTALIASES;
