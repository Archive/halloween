#!/usr/bin/perl -w

use strict;

$ENV{PATH} = '/bin:/usr/bin';
$ENV{ENV} = '';

require Mail::Send;

use Halloween;
use Sunrise;
use MIME::Base64;
use POSIX;
use Carp;

my $h = new Halloween;
my $s = new Sunrise;

$s->workingdir ('/home/halloween/halloween/bugzilla.gnome.org');

$s->read_alias_map ('alias-map.txt');
$s->read_crashers_map ('crashers-map.txt');
$s->read_program_map ('program-map.txt');
$s->read_opsys_map ('opsys-map.txt');
$s->read_version_map ('version-map.txt');
$s->read_reject_list ('reject-list.txt');
$s->read_reject_program_list ('reject-program-list.txt');

$s->success_file ('bugreport-success.txt');

my @files;
my @output;

sub do_parse($) {
    my $contents = shift;

    my $message = eval { $h->parse_message (undef, $contents, 0) };
    return unless ref $message;

    my $bug = eval { $h->bug_from_message ($message) };
    return $bug;
}

sub do_output(*$) {
    my ($fh, $gnozilla) = @_;

    foreach ($gnozilla->messages) {
	warn $_;
	print $fh $_;
    }
    $gnozilla->clear_messages;

    if (defined $gnozilla->output) {
	print $fh "\n" . $gnozilla->output . "\n";
	$gnozilla->clear_output;
    }
}

sub do_autoreply($$$;$) {
    my ($bug, $halloween, $sunrise, $bccbkor) = @_;

    my $subject = decode_base64($bug->{subject});

    my $msg = new Mail::Send;
    $msg->to($bug->{email});
    $msg->subject("Output of your command '$subject'");
    $msg->add('X-Loop','halloween@bugzilla.gnome.org');
    $msg->add('Precedence','junk');
    $msg->add('Reply-To','bugmaster@gnome.org');
    $msg->add('Errors-To','unknown@bugzilla.gnome.org');
    if ($bccbkor) {
        $msg->bcc('bugbuddy@bkor.dhs.org');
    }

    my $fh = $msg->open ('sendmail');
    do_output ($fh, $halloween);
    do_output ($fh, $sunrise);
    $fh->close;
}

my $contents = join '', <STDIN>;
my $bug = do_parse ($contents);

unless (ref $bug) {
    do_output (\*STDOUT, $h);
    exit 1;
}

unless (defined $bug->{email}) {
    # This is already enforced by Halloween, but we wanna be sure ...
    do_output (\*STDOUT, $h);
    exit 1;
}

my $retval = $s->sunrise ($bug);
if ($retval) {
    do_autoreply ($bug, $h, $s, $retval);
    exit ($retval == 2 ? 0 : 1);
}

do_autoreply ($bug, $h, $s);

my $msg = new Mail::Send;
$msg->to('bugzilla@bugzilla.gnome.org');
$msg->add('X-Loop','halloween@bugzilla.gnome.org');
$msg->add('Reply-To','bugmaster@gnome.org');
$msg->add('Errors-To','bugmaster@gnome.org');

my $fh = $msg->open ('sendmail');

print $fh qq[<?xml version="1.0" ?>\n];
print $fh qq[<!DOCTYPE bugzilla SYSTEM 'http://bugzilla.gnome.org/bugzilla.dtd'>\n];
print $fh $s->output_xml . "\n";

$fh->close;

exit 0;


