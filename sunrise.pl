#!/usr/bin/perl -w

use strict;

$ENV{PATH} = '/bin:/usr/bin';
$ENV{ENV} = '';

require Mail::Send;

use Halloween;
use Sunrise;
use Data::Dumper;
use POSIX;
use Carp;

my $h = new Halloween;
my $s = new Sunrise;

my @files;
my @output;

$s->workingdir (getcwd . '/bugzilla.gnome.org');

$s->read_alias_map ('alias-map.txt');
$s->read_opsys_map ('opsys-map.txt');
$s->read_version_map ('version-map.txt');
$s->read_reject_list ('reject-list.txt');

$s->exporter ('debbugs-export@bugzilla.gnome.org');

my $bugs = $h->parse_xml ('<STDIN>', join '', <STDIN>);

foreach ($h->messages) {
    warn "$_";
}
$h->clear_messages;

my $bug_count = 0;

my $bug;
foreach $bug (@$bugs) {
    if (!$s->sunrise ($bug)) {
	$bug_count++;
    }
}

foreach ($s->messages) {
    warn "$_";
}
$s->clear_messages;

exit unless $bug_count > 0;

my $msg = new Mail::Send;
$msg->to('bugzilla@bugzilla.gnome.org');

my $fh = $msg->open;

print $fh qq[<?xml version="1.0" ?>\n];
print $fh qq[<!DOCTYPE bugzilla SYSTEM 'http://bugzilla.gnome.org/bugzilla.dtd'>\n];
print $fh $s->output_xml . "\n";

$fh->close;
