package Sunrise;
use Gnozilla;
use strict;

use File::Basename;
use Mail::Address;
use MIME::Base64;
use MIME::Parser;
use XML::Parser;
use XML::Generator 0.8;
use Data::Dumper;
use Date::Parse;
use POSIX;
use Carp;

require "find-traces.pl";

use vars qw[$VERSION @ISA];

@ISA = ("Gnozilla");
$VERSION = 0.01;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self  = Gnozilla->new (@_);

    $self->{XML} = XML::Generator->new('escape' => 'always',
				       'conformance' => 'strict',
				       'pretty' => 2
				       );
    $self->{BUGS} = [];

    $self->{ALIASMAP} = [];
    $self->{PROGRAMMAP} = [];
    $self->{PROGRAMMAP_PRODUCTS} = {};
    $self->{CRASHERSMAP} = {};
    $self->{OPSYSMAP} = [];
    $self->{VERSIONMAP} = [];
    $self->{REJECTLIST} = [];
    $self->{REJECTPROGRAMLIST} = [];

    $self->{SUCCESSFILE} = undef;

    $self->{EXPORTER} = 'unknown@gnome.bugs';

    bless ($self, $class);
    return $self;
}

sub exporter {
    my ($self, $exporter) = @_;

    if (defined $exporter) {
	return $self->{EXPORTER} = $exporter;
    } else {
	return $self->{EXPORTER};
    }
}

sub get_email($$) {
    my ($self, $mail) = @_;

    if (ref $mail) {
	return $mail->[1];
    } else {
	return $mail;
    }
}

sub try_parse_bug_buddy($$) {
    my ($self, $id, $contents) = @_;

    my %bug_buddy;

    $contents = decode_base64 ($contents);

    if ($contents =~ /^Backtrace was generated from ?\n?'([-A-Za-z0-9._\/]+)'/sm ) {
        $bug_buddy{program} = $1;

        $bug_buddy{functions} = join(' ', get_traces_from_string($contents));
    }
    # Remove repeating '(no debugging symbols found)'
    $contents =~ s/(?:\(no debugging symbols found\)\n)+/(no debugging symbols found)\n/g;
    
    my @lines = split '\n', $contents;
    my $first = 1;
    foreach (@lines) {
	if (/^\s*$/) {
	    if ($first) {
		$first = 0;
	    } else {
		last;
	    }
	    next;
	}
	next if (/^Gnome distributor:.*$/);
	last unless /^([\w\-]+)\.*:\s*(.*?)$/;

	my ($key, $value) = ($1,$2);
	$key =~ tr/A-Z/a-z/;
	$value =~ s/^\s*//;
	$value =~ s/\s*$//;

	if ($key eq 'package') {
	    $bug_buddy{package} = $value;
	} elsif ($key eq 'severity') {
	    $bug_buddy{severity} = $value;
	} elsif ($key eq 'version') {
	    $bug_buddy{version} = $value;
	} elsif ($key eq 'system') {
	    $bug_buddy{system} = $value;
	} elsif ($key eq 'distribution') {
	    $bug_buddy{distribution} = $value;
	} elsif ($key eq 'platform') {
	    $bug_buddy{platform} = $value;
	} elsif ($key eq 'bugzilla-product') {
	    $bug_buddy{bugzilla_product} = $value;
	} elsif ($key eq 'bugzilla-component') {
	    $bug_buddy{bugzilla_component} = $value;
	} elsif ($key eq 'bugzilla-version') {
	    $bug_buddy{bugzilla_version} = $value;
	}
    }

    return {%bug_buddy};
}

sub parse_message($\@$) {
    my ($self, $id, $msg, $is_extra) = @_;

    my %message;
    my @attachments;

    my ($header, $contents);

    my $type = $msg->{type};

    $message{who} = $self->get_email ($msg->{originator}) if defined $msg->{originator};
    if (defined $msg->{date}) {
	$message{bug_when} = strftime "%Y-%m-%d %T", gmtime $msg->{date};
    }

    $contents = decode_base64 ($msg->{contents}) if defined $msg->{contents};
    $header = decode_base64 ($msg->{headers}) if defined $msg->{headers};

    if (defined $header and defined $contents) {
	my $parser = new MIME::Parser;
	$parser->decode_headers(0);
	$parser->output_to_core(1);
	$parser->ignore_errors(0);
	$parser->extract_nested_messages(0);

	my $data = $header . "\n" . $contents;

	my $entity = eval { $parser->parse_data ($data); };
	my $error = $@;
	if (!defined $entity) {
	    warn "$id: cannot parse message ($error)";
	    next;
	}

	my $num_parts = $entity->parts;
	my $bug_contents;

	if ($num_parts == 0) {
	    my $mimetype = $entity->effective_type;

	    if ($mimetype ne 'text/plain') {
		warn "$id: message has unknown mimetype `$mimetype'";
		next;
	    }

	    if ($is_extra) {
		my $text = '';
		foreach (qw[Subject From To Cc Message-Id Date]) {
		    next unless defined $entity->head->get($_);
		    $text .= $_ . ': ' . $entity->head->get($_);
		}

		$bug_contents = $text . "\n" . $entity->bodyhandle->as_string;
	    } else {
		$bug_contents = $entity->bodyhandle->as_string;
	    }
	    $bug_contents =~ s/\s*$/\n/;
	} else {
	    my $first_part = $entity->parts (0);
	    my $mimetype = $first_part->effective_type;

	    my $start;

	    unless ($is_extra) {
		if ($mimetype ne 'text/plain') {
		    warn "$id: message has unknown mimetype `$mimetype'";
		    next;
		}

		$bug_contents = $first_part->bodyhandle->as_string;
		$start = 1;
	    } else {
		$start = 0;
	    }

	    for ($id = $start; $id < $num_parts; $id++) {
		my $this_part = $entity->parts ($id);
		my $mimetype = $this_part->effective_type;

		my $filename = $this_part->head->recommended_filename;
		my $description = $this_part->head->mime_attr ("content-description");

		my %attach;
		$attach{who} = $message{who};
		$attach{bug_when} = $message{bug_when};
		$attach{desc} = $description if defined $description;
		$attach{filename} = $filename if defined $filename;
		$attach{mimetype} = $mimetype if defined $mimetype;
		if (defined $mimetype) {
		    $attach{is_patch} = $mimetype eq 'text/x-patch' ? 1 : 0;
		}
		$attach{data} = encode_base64 ($this_part->bodyhandle->as_string);

		if ($mimetype eq 'text/plain') {
		    next if $this_part->bodyhandle->as_string =~ /^\s*$/;
		}

		my @attach;
		foreach my $field (qw[attach_id who date desc filename is_patch mimetype
				      data]) {
		    push @attach, $self->{XML}->$field ($attach{$field}) if defined $attach{$field};
		}
		
		push @attachments, $self->{XML}->attachment ({encoding => 'base64'}, @attach);
	    }
	}

	if ($bug_contents) {
	    $message{thetext} = encode_base64 ($bug_contents);
	}

	return [\%message, @attachments];
    }

    return undef;
}



sub sunrise {
    my ($self, $bug) = @_;

    my %bug;

    my $id = (defined $bug->{id}) ? $bug->{id} : 0;
    $bug{bug_id} = $id;

    if (defined $bug->{done}) {
	$bug{resolution} = 'FIXED';
	$bug{bug_status} = 'CLOSED';
    }

    $bug{product} = $bug->{package} if defined $bug->{package};
    $bug{short_desc} = (defined $bug->{subject}) ? $bug->{subject} :
	encode_base64 ('');
    if (defined $bug->{originator}) {
	$bug{reporter} = $self->get_email ($bug->{originator});
    } elsif (defined $bug->{email}) {
	$bug{reporter} = $bug->{email};
    }

    if (defined $bug->{severity}) {
	if (lc($bug->{severity}) eq 'normal') {
	    $bug{bug_severity} = 'Normal';
	} elsif (lc($bug->{severity}) eq 'grave'
		 || lc($bug->{severity}) eq 'major') {
	    $bug{bug_severity} = 'Major';
	} elsif (lc($bug->{severity}) eq 'critical') {
	    $bug{bug_severity} = 'Critical';
	} elsif (lc($bug->{severity}) eq 'minor') {
	    $bug{bug_severity} = 'Minor';
	} elsif (lc($bug->{severity}) eq 'trivial') {
	    $bug{bug_severity} = 'Trivial';
	} elsif (lc($bug->{severity}) eq 'wishlist'
		 || lc($bug->{severity}) eq 'enhancement') {
	    $bug{bug_severity} = 'Enhancement';
	} else {
	    warn "$id: unknown severity `$bug->{severity}', setting to Normal";
	    $bug{bug_severity} = 'Normal';
	}
    }

    if (defined $bug->{date}) {
	$bug{creation_ts} = strftime "%Y-%m-%d %T", gmtime $bug->{date};
    }

    if (defined $bug->{message}) {
	my $retval = $self->parse_message ($id, $bug->{message}, 0);

	unless (ref $retval) {
	    warn "$id: can't parse message";
	    next;
	}

	my ($message, @attachments) = @$retval;

	if (!defined $message->{thetext}) {
	    warn "$id: can't get bug contents";
	    next;
	}

	my $bug_buddy = $self->try_parse_bug_buddy ($id, $message->{thetext});
	if (ref $bug_buddy) {
            if (defined $bug_buddy->{program}) {
                $bug{program} = $bug_buddy->{program};
            }
            if (defined $bug_buddy->{functions}) {
                $bug{functions} = $bug_buddy->{functions};
            }
	    if (defined $bug_buddy->{bugzilla_product}) {
		$bug{product} = $bug_buddy->{bugzilla_product};
		$bug{product_ok} = 1;
	    }
	    if (defined $bug_buddy->{bugzilla_component}) {
		$bug{component} = $bug_buddy->{bugzilla_component};
		$bug{product_ok} = 1;
	    }
	    if (defined $bug_buddy->{bugzilla_version}) {
		$bug{version} = $bug_buddy->{bugzilla_version};
		$bug{version_ok} = 1;
	    }
	    if (defined $bug_buddy->{version}) {
		$bug{version_details} = $bug_buddy->{version};
	    }
	    #added for gnome2.0 [louie@ximian.com]
	    if (defined $bug_buddy->{gnomeversion}) {
		$bug{keywords} = 'GNOME2';
	    }
	    if (defined $bug_buddy->{system}) {
		$bug{op_sys_details} = $bug_buddy->{system};
		if (defined $bug_buddy->{distribution}) {
		    $bug{op_sys_details} .= ' - '. $bug_buddy->{distribution};
		}
	    } elsif (defined $bug_buddy->{platform}) {
		$bug{op_sys_details} = $bug_buddy->{platform};
	    }
	  }

	if (defined $bug{op_sys_details}) {
	    foreach (@{$self->{OPSYSMAP}}) {
		if ($self->match ($bug{op_sys_details}, $_->[1])) {
		    $bug{op_sys} = $_->[0];
		    last;
		}
	    }
	}
	if (!defined $bug{op_sys_details} || $bug{op_sys_details} =~ /^\s*$/) {
	    if ($bug{product} eq 'gimp') {
		$bug{op_sys} = 'All';
	    }
	}

	if (ref $bug{long_desc}) {
	    push @{$bug{long_desc}}, $message;
	} else {
	    $bug{long_desc} = [$message];
	}
	
	if (ref $bug{attachments}) {
	    push @{$bug{attachments}}, @attachments;
	} else {
	    $bug{attachments} = [@attachments];
	}
    }

    if (defined $bug->{extra}) {
	my @extras = @{$bug->{extra}};

	my $extra;
	foreach $extra (@{$bug->{extra}}) {
	    my $retval = $self->parse_message ($id, $extra, 1);

	    unless (ref $retval) {
		warn "$id: can't parse message";
		next;
	    }

	    my ($message, @attachments) = @$retval;

	    if (ref $bug{attachments}) {
		push @{$bug{attachments}}, @attachments;
	    } else {
		$bug{attachments} = [@attachments];
	    }

	    if (defined $message->{thetext}) {
		if (ref $bug{long_desc}) {
		    push @{$bug{long_desc}}, $message;
		} else {
		    $bug{long_desc} = [$message];
		}
	    }
	}
    }

    if (!defined $bug{bug_status}) {
	$bug{bug_status} = 'UNCONFIRMED';
    }

    if (!defined $bug{bug_severity}) {
	$bug{bug_severity} = 'Normal';
    }

    # Map debbugs packages to Bugzilla products / components
    if (!defined $bug{product_ok}) {
	foreach (@{$self->{ALIASMAP}}) {
	    if ($self->match ($bug{product}, $_->[0])) {
		$bug{product} = $_->[1];
		$bug{component} = $_->[2];
		$bug{version} = $_->[3] if defined $_->[3];
		last;
	    }
	}
    }

    if (!defined $bug{version_ok}) {
	if (!defined $bug{version} and defined $bug{version_details}) {
	    foreach (@{$self->{VERSIONMAP}}) {
		if ($self->match ($bug{product}, $_->[0]) and
		    $self->match ($bug{version_details}, $_->[1])) {
		    $bug{version} = $_->[2];
		    last;
		}
	    }
	}
    }

    # Forcefully move bugs based on the program that crashed
    if (exists $bug{program} and defined $self->{PROGRAMMAP_PRODUCTS}->{$bug{product}} ) {
	foreach (@{$self->{PROGRAMMAP}}) {
	    if ($self->match ($bug{program}, $_->[0]) || $self->match (basename($bug{program}), $_->[0])) {
                
                if ( $bug{product} eq $_->[1] ) {
                    # If product and components are the same, do nothing
                    if ($_->[2] && $bug{component} eq $_->[2]) {
                        last;
                    }
                    
                    # Now check the move_if_products_are_the_same flag
                    if ( !defined($_->[3])      # if flag isn't given
                         || !$_->[3]) {         #   or flag is false
                        # Do not move the bug (flag it isn't set)
                        next;
                    }
                }
                
                $bug{orig_product} = $bug{product};
                $bug{orig_component} = $bug{component};
		$bug{product} = $_->[1];
		$bug{component} = $_->[2];
		last;
	    }
	}
    }

    if (exists $bug{functions} and defined $self->{CRASHERSMAP}->{$bug{functions}} ) {
        my ($bugnr, $verregexp, $prod) = @{$self->{CRASHERSMAP}->{$bug{functions}}};

        # product is optional. If it is specified, it MUST match $bug{product}
        
        if ($self->match ($bug{version_details}, $verregexp)
            && (!$prod || $self->match ($bug{product}, $prod)) ) {
            my $func_rej_file = "reject-fixed-crasher.txt";
            
            open MESSAGEFILE, $self->{WORKINGDIR} . "/" . $func_rej_file or do {
                $self->whine ("open ($func_rej_file): $!");
                return 2;
            };
            
            my $msg = join '', <MESSAGEFILE>;
            close MESSAGEFILE;
            
            $msg =~ s/BUGNR/$bugnr/g;
            $self->output ($msg); 
            return 2;
        }
    }
    
    $bug{priority} = 'Normal';
    $bug{component} = 'general' unless defined $bug{component};
    $bug{version} = 'unspecified' unless defined $bug{version};
    $bug{op_sys} = 'other' unless defined $bug{op_sys};

    my @fields;
    foreach my $field (qw[bug_status resolution product priority version version_details component reporter keywords
			  bug_severity creation_ts op_sys op_sys_details short_desc program orig_product 
                          orig_component]) {
	if (defined $bug{$field}) {
	    if ($field eq 'short_desc') {
		push @fields, $self->{XML}->$field ({encoding => 'base64'}, $bug{$field});
	    } else {
		push @fields, $self->{XML}->$field ($bug{$field});
	    }
	}
    }

    if (ref $bug{long_desc}) {
	foreach my $long_desc (@{$bug{long_desc}}) {
	    my @long_desc;
	    foreach my $field (qw[who bug_when thetext]) {
		if (defined $long_desc->{$field}) {
		    push @long_desc, $self->{XML}->$field ($long_desc->{$field});
		}
	    }
	    push @fields, $self->{XML}->long_desc ({encoding => 'base64'}, @long_desc);
	}
    }

    if (ref $bug{attachments}) {
	foreach my $attachment (@{$bug{attachments}}) {
	    push @fields, $attachment;
	}
    }

    my $bug_prefix = ($bug{bug_id} > 0) ? sprintf ("\#%d: ", $bug{bug_id}) : "";

    foreach (@{$self->{REJECTLIST}}) {
	if ($self->match ($bug{product}, $_->[0])) {
	    my ($filename, $retval) = ($_->[1], 1);

	    if (substr ($filename, 0, 1) eq '-') {
		$filename = substr ($filename, 1);
		$retval = 2;
	    }

	    $self->output_file ($filename);
	    $self->whine ($bug_prefix . "Package '".$bug{product}."' rejected");
	    return $retval;
	}
    }

    # Reject bugreports based upon the program that crashed
    if (exists $bug{program}) {
        foreach (@{$self->{REJECTPROGRAMLIST}}) {
            if ($self->match ($bug{program}, $_->[0]) || $self->match (basename($bug{program}), $_->[0])) {
                my ($filename, $retval) = ($_->[1], 1);

                if (substr ($filename, 0, 1) eq '-') {
                    $filename = substr ($filename, 1);
                    $retval = 2;
                }

                $self->output_file ($filename);
                $self->whine ($bug_prefix . "Program '".$bug{program}."' rejected");
                return $retval;
            }
        }
    }

    $self->message ("notice",
		    $bug_prefix.
		    "Successfully parsed bug report: ".$bug{product}." / ".
		    $bug{component}." / ".$bug{version}.".");

    push @{$self->{BUGS}}, $self->{XML}->bug ({bug_id => $bug{bug_id}}, @fields);
    $self->output_file ($self->{SUCCESSFILE}) if defined $self->{SUCCESSFILE};
    return 0;
}

sub output_xml($) {
    my $self = shift;

    my $bugzilla = $self->{XML}->bugzilla ({version => '2.10',
					    exporter => $self->{EXPORTER},
					    urlbase => 'http://bugs.gnome.org/',
					    maintainer => 'bugmaster@gnome.org'},
					   @{$self->{BUGS}});

    $self->{BUGS} = [];

    return $bugzilla;
}

sub success_file($$) {
    my ($self, $filename) = @_;

    $self->{SUCCESSFILE} = $filename;
}

sub read_crashers_map($$) {
    my ($self, $filename) = @_;

    open CRASHERSMAP, $self->{WORKINGDIR} . "/" . $filename or
	$self->error ("open ($filename): $!");
    while (<CRASHERSMAP>) {
        chomp;
	next if /^\#/;
	next if /^\s*$/;

	my ($bugnr, $verregexp, $f1, $f2, $f3, $f4, $f5, $product) = split ' ';

	undef $bugnr if defined $bugnr and $bugnr =~ /^\s*$/;
	undef $verregexp if defined $verregexp and $verregexp =~ /^\s*$/;
	undef $f5 if defined $f5 and $f5 =~ /^\s*$/;
        undef $product if defined $product and $product =~ /^\s*$/;

        # Require 5 functions
        next unless $f5;

        # Bugnr must be a numeric
        next unless $bugnr =~ /^[0-9]+$/;
        
        my $funcstring = join(' ', ($f1, $f2, $f3, $f4, $f5));
        
	$self->{CRASHERSMAP}->{$funcstring} = [$bugnr,$verregexp, $product];
    }
    close CRASHERSMAP;
}

sub read_alias_map($$) {
    my ($self, $filename) = @_;

    open ALIASMAP, $self->{WORKINGDIR} . "/" . $filename or
	$self->error ("open ($filename): $!");
    while (<ALIASMAP>) {
	next if /^\#/;
	next if /^\s*$/;

	my ($package, $product, $component, $version) = split ' ';

	undef $product if defined $product and $product =~ /^\s*$/;
	undef $component if defined $component and $component =~ /^\s*$/;
	undef $version if defined $version and $version =~ /^\s*$/;

	$product = $package unless defined $product;
	$component = 'general' unless defined $component;
#	$version = 'unspecified' unless defined $version;

	push @{$self->{ALIASMAP}}, [$package,$product,$component,$version];
    }
    close ALIASMAP;
}

sub read_program_map($$) {
    my ($self, $filename) = @_;

    open PROGRAMMAP, $self->{WORKINGDIR} . "/" . $filename or
	$self->error ("open ($filename): $!");
    while (<PROGRAMMAP>) {
	next if /^\#/;
	next if /^\s*$/;
        chomp;

	my ($program, $product, $component, $move_if_in_product) = split /\t/;

	undef $product if defined $product and $product =~ /^\s*$/;
	undef $component if defined $component and $component =~ /^\s*$/;
	undef $move_if_in_product if defined $move_if_in_product and $move_if_in_product =~ /^\s*$/;

	$component = 'general' unless defined $component;
        $move_if_in_product = '0' unless defined $move_if_in_product;

	push @{$self->{PROGRAMMAP}}, [$program,$product,$component,$move_if_in_product];
        $self->{PROGRAMMAP_PRODUCTS}->{$product} = 1;
    }
    close PROGRAMMAP;
}

sub read_opsys_map($$) {
    my ($self, $filename) = @_;

    open OPSYSMAP, $self->{WORKINGDIR} . "/" . $filename or
	$self->error ("open ($filename): $!");
    while (<OPSYSMAP>) {
	next if /^\#/;
	next if /^\s*$/;

	do {
	    $self->warn ("Illegal opsys line `$_'");
	    next;
	} unless /^(.*?)\s+(.*?)\s*$/;

	push @{$self->{OPSYSMAP}}, [$1,$2];
    }
    close OPSYSMAP;
}

sub read_version_map($$) {
    my ($self, $filename) = @_;

    open VERSIONMAP, $self->{WORKINGDIR} . "/" . $filename or
	$self->error ("open ($filename): $!");
    while (<VERSIONMAP>) {
	next if /^\#/;
	next if /^\s*$/;
	chop;

	my ($product, $regexp, $version) = split /\t/;

	push @{$self->{VERSIONMAP}}, [$product,$regexp,$version];
    }
    close VERSIONMAP;
}

sub read_reject_list($$) {
    my ($self, $filename) = @_;

    open REJECTLIST, $self->{WORKINGDIR} . "/" . $filename or
	$self->error ("open ($filename): $!");
    while (<REJECTLIST>) {
	next if /^\#/;
	next if /^\s*$/;
	chop;

	my ($regexp, $autoreplyfile) = /^(.*?)\s*\t\s*(.*?)\s*$/;

	push @{$self->{REJECTLIST}}, [$regexp,$autoreplyfile];
    }
    close REJECTLIST;
}

sub read_reject_program_list($$) {
    my ($self, $filename) = @_;

    open REJECTPROGRAMLIST, $self->{WORKINGDIR} . "/" . $filename or
	$self->error ("open ($filename): $!");
    while (<REJECTPROGRAMLIST>) {
	next if /^\#/;
	next if /^\s*$/;
	chop;

	my ($regexp, $autoreplyfile) = /^(.*?)\s*\t\s*(.*?)\s*$/;

	push @{$self->{REJECTPROGRAMLIST}}, [$regexp,$autoreplyfile];
    }
    close REJECTPROGRAMLIST;
}

1;
