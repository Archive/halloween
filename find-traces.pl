# -*- Mode: perl; indent-tabs-mode: nil -*-
use diagnostics;
use strict;

=head1 get_traces_from_string

takes a string that may contain a stack trace. If it does contain
a viable stack trace, the first five useful functions are returned.

=cut

sub get_traces_from_string {
  my ($string) = @_;
  my @functions = ();
  my $function_index = -1;
  my @lines = split('\n', $string);

  # For a simplification, all of this could be done in one regexp. [Ben Frantzdale]
  # You volunteering to write it, Ben? :) [Luis 'not great with regexp' Villa]

  foreach my $line (@lines) {
  # Here, we try to ignore things before <signal handler> and/or killpg()
    if ( $line =~ /<signal handler called>|killpg|sigaction|sigsuspend|\(gdb\) bt|gnome_init|g_log/ ) {
      $function_index = 0; #Now we've started looking for real functions, or reset after reading g_log tops
    }  #here, we get only function names after the garbage
    elsif ( $line =~ /^#\d+ +0x[0-9A-F]+ in ((\w|::|_)+) \(/i &&
	    $function_index >= 0){
        if($function_index == 0 ||
           $functions[$function_index-1] ne $1){ #test to eliminate dups
            $functions[$function_index]=$1;
            $function_index++;
        }
    }
    # We stop after five functions are found:
    last if $function_index > 4;
  }

  #did we go all the way through without getting any frames?
  #if so, we've got a weird trace; we'll pick it up ourselves [since we take for granted there is actually a trace there]
  if ( $function_index == -1 ) {
      foreach my $line (@lines) {
          if ( $line =~ /^#\d+ +0x[0-9A-F]+ in ((\w|::|_)+) \(/i ){
              push(@functions, $1);
              $function_index++;
          }
          # We stop after five functions are found:
          last if $function_index > 3; # only go to three because we started at -1
      }
  }

  return @functions;
}





=head1 get_long_description_from_database

takes a bug number and returns the long description of the bug.

=cut

sub get_long_description_from_database {
  my ($bugnum) = @_;
  $bugnum =~ s/(\d+)/$1/;

#use the experimental regexp here ;)
  my $query = "
    SELECT
      thetext
    FROM
      longdescs
    WHERE
      bug_id='$bugnum'
    AND
      thetext REGEXP \"#[[:digit:]]+ +0x[0-9A-F]+ in \([[:alnum:]]+\)\"
    ORDER BY
      bug_when DESC
    LIMIT 1
    ";

  SendSQL ($query);

  my $result = "";
  while (my ($text) = FetchSQLData()) {
    $result .= $text;
  }
  return $result;
}

# WordListQuery was modified from buglist.cgi (GetByWordListSubstr)
sub WordListQuery {
    my ($field, @strs) = (@_);
    my @list;

    foreach my $word (@strs) {
        if ($word ne "") {
            push(@list, "INSTR(LOWER($field), " . lc(SqlQuote($word)) . ")");
        }
    }

    return \@list;
}

=head1 show_duplicates_given_functions

takes a list of functions to be found in stack traces and returns
a list of bugs which match.

=cut
sub show_duplicates_given_functions {
my @functions = @_ ;

#Ben had been using a left join here, but it didn't work for some reason so I took it out. [Luis]
#We tried to originally judge order by just directly searching using REGEXP but mysql is sloooow
# for that case

  my $query;
  $query = "
    SELECT DISTINCT
      bugs.bug_id,
      substring(bugs.bug_status,1,4),
      substring(bugs.resolution,1,4),
      substring(bugs.short_desc, 1, 60)
    FROM 
      bugs, longdescs
    WHERE
      bugs.bug_id = longdescs.bug_id
    AND ";

  $query .= join(" AND ", @{WordListQuery("longdescs.thetext", @functions)});

  # Group by id and limit to 100 (if we end up getting just one function that
  # appears in all bugs, then the list will be uselessly ridiculously long
  # and put a heavy load on the server too.  Lists become uselessly long far
  # before 100 anyway...
  $query .= "
    LIMIT
      100
    ";

  #print "<p>$query</p>";

  SendSQL ($query);

  print "<table border=0 cellspacing=0 cellpadding=5>\n";

  # XXX - Replace this with a DBI function in Bugzilla 2.18+
  my $nr = 0;
  
  while (my ($bug_id, $status, $resolution, $desc) = FetchSQLData() ) {
    $nr++;
    print "  <tr>\n";
    print "    <td align=\"right\">" .
          "      <a href=../show_bug.cgi?id=$bug_id#stacktrace>$bug_id</a></td>\n";
    print "    <td>$status</td>\n";
    print "    <td>$resolution</td>\n";
    print "    <td>$desc</td>\n";
    print "  </tr>\n";
  }
  print "</table>\n";

  return ($nr == "100");
}

#this is total crack, but it makes the perl+apache stuff happy- don't delete
1;
