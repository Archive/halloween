package Gnozilla;
use strict;

use vars qw[$VERSION];

$VERSION = 0.01;

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self  = {};

    $self->{MESSAGES} = [];
    $self->{OUTPUT} = undef;
    $self->{WORKINGDIR} = '.';

    bless ($self, $class);
    return $self;
}

sub workingdir {
    my ($self, $workingdir) = @_;

    if (defined $workingdir) {
	return $self->{WORKINGDIR} = $workingdir;
    } else {
	return $self->{WORKINGDIR};
    }
}

sub messages { 
    @{shift->{MESSAGES}};
}

sub clear_messages {
    my $self = shift;
    $self->{MESSAGES} = [];
}

sub message {
    my $self = shift;
    my $type = shift;
    my @args = map { defined($_) ? $_ : '<<undef>>' } @_;

    push @{$self->{MESSAGES}}, ($type.": ".join('', @args)."\n");
}

sub error {
    my $self = shift;
    $self->message('error', @_);
    die @_;
}

sub whine {
    my $self = shift;
    $self->message('warning', @_);
}

sub output {
    my ($self, $output) = @_;

    if (defined $output) {
	$self->{OUTPUT} = '' unless defined $self->{OUTPUT};
	$self->{OUTPUT} .= $output;
    } else {
	return $self->{OUTPUT};
    }
}

sub clear_output {
    my $self = shift;
    undef $self->{OUTPUT};
}

sub output_file {
    my ($self, $filename) = @_;

    open MESSAGEFILE, $self->{WORKINGDIR} . "/" . $filename or do {
	$self->whine ("open ($filename): $!");
	return;
    };
    $self->output (join '', <MESSAGEFILE>);
    close MESSAGEFILE;
}

sub match {
    my ($self, $string, $regexp) = @_;

    if ($regexp =~ m,^[m/],) {
	return eval "\$string =~ $regexp";
    } else {
	return $string eq $regexp;
    }
}

1;
