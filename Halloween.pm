package Halloween;
use Gnozilla;
use strict;

use Mail::Address;
use MIME::Base64;
use MIME::Parser;
use XML::Parser;
use XML::Generator 0.8;
use Data::Dumper;
use Date::Parse;
use Carp;

use vars qw[$VERSION @ISA];

$VERSION = 0.01;
@ISA = ("Gnozilla");

sub new {
    my $proto = shift;
    my $class = ref($proto) || $proto;
    my $self  = Gnozilla->new (@_);

    $self->{SPOOLDIR} = '.';

    bless ($self, $class);
    return $self;
}

sub spooldir {
    my $self = shift;
    if (@_) { $self->{SPOOLDIR} = shift }
    return $self->{SPOOLDIR};
}

sub sani($$) {
    my ($self, $in) = @_;

    my %saniarray = ('<','lt', '>','gt', '&','amp', '"','quot');

    my $out;
    while ($in =~ m/[<>&"]/) {
        $out.= $`. '&'. $saniarray{$&}. ';';
        $in=$';
    }
    $out.= $in;
    $out;
}

sub parse_email($$) {
    my ($self, $mail) = @_;

    $mail =~ s/\s*$//; $mail =~ s/^\s*//;
    my @addrs = Mail::Address->parse ($mail);
    if ($#addrs != 0) {
	if ($#addrs > 0) {
	    $self->whine ("Only using first email address from `$mail'");
	} else {
	    $self->error ("Cannot parse email `$mail'");
	}
    }

    my $address = $addrs[0]->address;
    my $name = $addrs[0]->name;

    if (defined $name) {
	return [encode_base64 ($name),$address];
    } else {
	return $address;
    }
}

sub parse_log_message($$$$$) {
    my ($self, $filename,$bug,$submit,$message) = @_;

    # Drop submit@bugs.gnome.org since we already have this
    # from the <ID>.report file.
    return if $submit eq 'submit@bugs.gnome.org';

    my $extra = $self->parse_message ($filename, $message);
    return unless ref $extra;

    if ($submit =~ /^\d+(-quiet|-maintonly)?\@bugs\.gnome\.org$/) {
	$extra->{type} = 'followup';
    } elsif ($submit =~ /^(bugs|maintonly)\@bugs\.gnome\.org$/) {
	$extra->{type} = 'followup';
    } elsif ($submit =~ /^\d+-(done|close)\@bugs\.gnome\.org$/) {
	$extra->{type} = 'close';
    } else {
	warn "$filename: Unknown submit address $submit";
    }

    if (ref $bug->{extra}) {
	push @{$bug->{extra}}, $extra;
    } else {
	$bug->{extra} = [$extra];
    }
}

sub parse_log($$$) {
    my ($self, $filename,$bug) = @_;

    open (L, $filename) or do {
	warn "open ($filename): $!";
	return;
    };

    my $normstate= 'kill-init';
    my $suppressnext = 0;
    my $this = '';

    my $received = '';

    while(<L>) {
        if (m/^\07$/) {
            $normstate eq 'kill-init' || $normstate eq 'kill-end' ||
                &quit("$filename ^G in state $normstate");
            $normstate = 'incoming-recv';
        } elsif (m/^\01$/) {
            $normstate eq 'kill-init' || $normstate eq 'kill-end' ||
                &quit("$filename ^A in state $normstate");
            $normstate = 'autocheck';
        } elsif (m/^\02$/) {
            $normstate eq 'kill-init' || $normstate eq 'kill-end' ||
                &quit("$filename ^B in state $normstate");
            $normstate = 'recips';
        } elsif (m/^\03$/) {
            $normstate eq 'go' || $normstate eq 'go-nox' || $normstate eq 'html' ||
                &quit("$filename ^C in state $normstate");
	    if (!$suppressnext && $normstate eq 'go') {
		$self->parse_log_message ($filename, $bug, $received, $this);
	    }
            $suppressnext = $normstate eq 'html';
            $normstate = 'kill-end';
	    $this = '';
        } elsif (m/^\05$/) {
            $normstate eq 'kill-body' || &quit("^E in state $normstate");
            $normstate = 'go';
        } elsif (m/^\06$/) {
            $normstate eq 'kill-init' || $normstate eq 'kill-end' ||
                &quit("$filename ^F in state $normstate");
            $normstate = 'html'; $this= '';
        } elsif ($normstate eq 'incoming-recv') {
            my $pl = $_; $pl =~ s/\n+$//;
            m/^Received: \(at (\S+)\) by (\S+)\;/ ||
                &quit("bad line \`$pl' in state incoming-recv");
	    $received = "$1\@$2";
            $normstate = 'go';
        } elsif ($normstate eq 'html') {
            $this .= $_;
        } elsif ($normstate eq 'go') {
            $this .= $_;
        } elsif ($normstate eq 'go-nox') {
            next if !s/^X//;
            $this .= $_;
        } elsif ($normstate eq 'recips') {
            $normstate = 'kill-body';
        } elsif ($normstate eq 'autocheck') {
            next if !m/^X-Debian-Bugs(-\w+)?: This is an autoforward from (\S+)/;
            $normstate= 'autowait';
        } elsif ($normstate eq 'autowait') {
            next if !m/^$/;
            $normstate= 'go-nox';
        } else {
            &quit("$filename state $normstate line \`$_'");
        }
    }
    &quit("$filename state $normstate at end") unless $normstate eq 'kill-end';
    close(L);
}


sub parse_message($$$$) {
    my ($self, $filename, $contents, $is_extra) = @_;

    unless (defined $contents) {
	if (defined $filename) {
	    open INPUT, $filename or $self->error ("open ($filename): $!");
	    $contents = join '', <INPUT>;
	    close INPUT;
	} else {
	    $filename = '<STDIN>';
	    $contents = join '', <STDIN>;
	}
    }

    my $discarding = $is_extra ? 'IGNORING extra section' : 'DISCARDING';

    my $parser = new MIME::Parser;
    $parser->decode_headers(0);
    $parser->output_to_core(1);
    $parser->ignore_errors(0);
    $parser->extract_nested_messages(0);
    my $entity = eval { $parser->parse_data ($contents); };
    unless (ref $entity) {
	$self->error ("Can't get mail headers (".$@.")");
    }

    my $head = $entity->head;
    $head->unfold;
    $head->decode;

    my (%message, @fields);

    if ($head->count('X-Mailer')) {
	$message{mailer} = $head->get('X-Mailer');
    }

    if ($head->count('Message-ID')) {
	$message{msgid} = encode_base64 ($head->get('Message-ID'));
    }

    # Don't use the Date: of the original mail as creation date
    # A lot of systems are running a year in the past
    $message{date} = time();

    if ($head->count('From')) {
	my $mail = $self->parse_email ($head->get('From'));

	if (ref $mail) {
	    $message{from} = $mail->[0];
	    $message{email} = $mail->[1];
	} else {
	    $message{email} = $mail;
	}
    } else {
	$self->error ("Email missing");
    }

    if ($head->count('Subject')) {
	$message{subject} = $head->get('Subject');
	chomp($message{subject});
	$message{subject} = encode_base64 ($message{subject});
    }

    $message{headers} = encode_base64 ($head->as_string);
    $message{contents} = encode_base64 ($entity->body_as_string);

    return {%message};
}

sub parse_bug_buddy($$$) {
    my ($self, $contents) = @_;

    my %bug_buddy;

    $contents = decode_base64 ($contents);
    my @lines = split '\n', $contents;
    my $first = 1;
    foreach (@lines) {
	if (/^\s*$/) {
	    if ($first) {
		$first = 0;
	    } else {
		last;
	    }
	    next;
	}
	last unless /^([\w\-]+)\.*:\s*(.*?)$/;

	if ($1 eq 'Package') {
	    $bug_buddy{package} = $2;
	} elsif ($1 eq 'Severity') {
	    $bug_buddy{severity} = $2;
	} elsif ($1 eq 'Version') {
	    $bug_buddy{version} = $2;
	} elsif ($1 eq 'System') {
	    $bug_buddy{system} = $2;
	} elsif ($1 eq 'Distribution') {
	    $bug_buddy{distribution} = $2;
	} elsif ($1 eq 'Platform') {
	    $bug_buddy{platform} = $2;
	    #added for gnome2.0 [louie@ximian.com]
	} elsif ($1 eq 'BugBuddy-GnomeVersion') {
	    $bug_buddy{gnomeversion} = $2;
	}
    }

    return {%bug_buddy};
}

sub output_message($$$) {
    my ($self, $xml, $message) = @_;

    my @fields;

    if (defined $message->{email} || defined $message->{from}) {
	my @originator;

	push @originator, $xml->email ($message->{email}) if defined $message->{email};
	push @originator, $xml->from ($message->{from}) if defined $message->{from};

	push @fields, $xml->originator (@originator);
    }

    foreach (qw[subject msgid mailer date headers contents]) {
	push @fields, $xml->$_ ($message->{$_}) if defined $message->{$_};
    }

    my $type = defined $message->{type} ? $message->{type} : 'submit';

    return $xml->message ({type => $type}, @fields);
}

sub output_bugreport($$$) {
    my ($self, $filename, $bug) = @_;

    my $xml = XML::Generator->new('escape' => 'always',
				  'conformance' => 'strict',
				  'pretty' => 2
				  );

    my @fields;

    if (!defined $bug->{email}) {
	warn "$filename: DISCARDING: missing email";
	return;
    }

    if (defined $bug->{from}) {
	push @fields, $xml->originator ($xml->email ($bug->{email}),
					$xml->from ($bug->{from}));
    } else {
	push @fields, $xml->originator ($xml->email ($bug->{email}));
    }

    foreach (qw[subject mailer msgid date package keywords severity]) {
	next unless defined $bug->{$_};
	next if $bug->{$_} =~ /^\s*$/;

	push @fields, $xml->$_ ($bug->{$_});
    }

    if (defined $bug->{done} && !$bug->{done} =~ /^\s*$/) {
	my $done = $self->parse_email ($bug->{done});
	if (ref $done) {
	    push @fields, $xml->done ($xml->email ($done->[1]),
				      $xml->from ($done->[0]));
	} else {
	    push @fields, $xml->done ($xml->email ($done));
	}
    }

    if (defined $bug->{forwarded} && !$bug->{forwarded} =~ /^\s*$/) {
	my $forwarded = $self->parse_email ($bug->{forwarded});
	if (ref $forwarded) {
	    push @fields, $xml->forwarded ($xml->email ($forwarded->[0]),
					   $xml->from ($forwarded->[1]));
	} else {
	    push @fields, $xml->forwarded ($xml->email ($forwarded));
	}
    }

    if (defined $bug->{mergedwith} && !$bug->{mergedwith} =~ /^\s*$/) {
	my @merged = split ' ', $bug->{mergedwith};

	my @ids;
	foreach (@merged) {
	    push @ids, $xml->id ($_);
	}

	push @fields, $xml->mergedwith (@ids);
    }

    if (!ref $bug->{message}) {
	warn "$filename: DISCARDING: missing actual bug report";
	return;
    }

    push @fields, $self->output_message ($xml, $bug->{message});

    if (ref $bug->{extra}) {
	my @extras;

	foreach (@{$bug->{extra}}) {
	    push @extras, $self->output_message ($xml, $_);
	}

	push @fields, $xml->extras (@extras);
    }

    my $output = $xml->bug({id => $bug->{id}},
			   @fields);


    return $output . "\n";
}


sub read_bugreport($$) {
    my ($self, $id) = @_;

    my $status_file = $self->{SPOOLDIR} . '/' . $id . '.status';
    my $report_file = $self->{SPOOLDIR} . '/' . $id . '.report';
    my $log_file = $self->{SPOOLDIR} . '/' . $id . '.log';

    my %bug;

    open (STATUSFILE, $status_file) or do {
	$self->error ("open ($status_file): $!");
    };

    $bug{id} = $id;

    chomp (my $originator = <STATUSFILE>);
    chomp($bug{date} = <STATUSFILE>);
    chomp($bug{subject} = <STATUSFILE>);
    chomp($bug{msgid} = <STATUSFILE>);
    chomp($bug{package} = <STATUSFILE>);
    chomp($bug{keywords} = <STATUSFILE>);
    chomp($bug{done} = <STATUSFILE>);
    chomp($bug{forwarded} = <STATUSFILE>);
    chomp($bug{mergedwith} = <STATUSFILE>);
    chomp($bug{severity} = <STATUSFILE>);


    $bug{subject} = encode_base64 ($bug{subject});

    $bug{package} = 'general' if $bug{package} =~ /^\s*$/;

    if (!$originator =~ /^\s*$/) {
	my $mail = $self->parse_email ($originator);
	if (ref $mail) {
	    $bug{from} = $mail->[0];
	    $bug{email} = $mail->[1];
	} else {
	    $bug{email} = $mail;
	}
    }

    close STATUSFILE;

    open (REPORTFILE, $report_file) or do {
	$self->error ("open ($report_file): $!");
    };
    my $report_contents = join '', <REPORTFILE>;
    close REPORTFILE;

    my $message = $self->parse_message ($id, $report_contents, 0);
    next unless ref $message;

    $bug{message} = $message;

    if (!defined $bug{email} or $bug{email} =~ /^\s*$/) {
	$bug{email} = $bug{message}->{email};
    }

    $self->parse_log ($log_file, \%bug);

    return {%bug};
}

sub bug_from_message($$) {
    my ($self, $message) = @_;

    return unless ref $message;

    my %bug;
    $bug{message} = $message;
    foreach (qw[email subject date mailer msgid]) {
	$bug{$_} = $message->{$_} if defined $message->{$_};
    }

    my $bug_buddy = $self->parse_bug_buddy ($message->{contents});
    carp "Can't parse bug-buddy" unless ref $bug_buddy;

    foreach (qw[package severity]) {
	$bug{$_} = $bug_buddy->{$_} if defined $bug_buddy->{$_};
    }

    $bug{subject} = '(no subject)' unless defined $bug{subject};

    return {%bug};
}

sub parse_xml($) {
    my ($self, $filename, $contents) = @_;

    my @bugs;

    unless (defined $contents) {
	open INPUT, $filename or $self->error ("open ($filename): $!");
	$contents = join '', <INPUT>;
	close INPUT;
    }

    $contents =~ s/^.+(<\?xml version.+)$/$1/s;

    my $parser = new XML::Parser(Style => 'Tree', Pkg => 'Halloween');
    my $tree = eval { $parser->parse ($contents) };
    unless (ref $tree) {
	$self->error ($@);
    }

    my $bugqty = ($#{@{$tree}->[1]} +1 -3) / 4;

    my $sub_parse_email = sub(@) {
	my ($email, $from);
	for (my $j=3 ;$j < $#_; $j=$j+4) {
	    my ($subkey, $subvalue) = ($_[$j], $_[$j+1]);

	    if ($subkey eq 'email' and defined $subvalue->[2]) {
		$email = $subvalue->[2];
	    }
		    
	    if ($subkey eq 'from' and defined $subvalue->[2]) {
		$from = $subvalue->[2];
	    }
	}

	if (defined $from) {
	    return [$from,$email];
	} else {
	    return $email;
	}
    };

    my $sub_parse_message = sub(@) {
	my %message;

	$message{type} = $_[0]->{type} if defined $_[0]->{type};

	for (my $j=3 ;$j < $#_; $j=$j+4) {
	    my ($subkey, $subvalue) = ($_[$j], $_[$j+1]);

	    if ($subkey eq 'originator') {
		$message{originator} = &$sub_parse_email (@$subvalue);
		if (ref $message{originator}) {
		    $message{email} = $message{originator}->[1];
		} else {
		    $message{email} = $message{originator};
		}
	    }

	    foreach (qw[subject msgid mailer date headers contents]) {
		if ($subkey eq $_ and defined $subvalue->[2]) {
		    $message{$_} = $subvalue->[2];
		}
	    }
	    
	}

	return {%message};
    };

    for (my $k=1 ; $k <= $bugqty ; $k++) {
	my $cur = $k*4;
	my $node = $tree->[1][$cur];

	my %bug;

	my $id;
	if (defined $node->[0]->{id}) {
	    $bug{id} = $id = $node->[0]->{id};
	} else {
	    $bug{id} = $id = 0;
	}

	my @nodes = @$node;
	
	for (my $i=3 ;$i < $#nodes; $i=$i+4) {
	    my ($key, $value) = ($node->[$i], $node->[$i+1]);

	    foreach (qw[subject mailer msgid date package keywords severity]) {
		if ($key eq $_ and defined $value->[2]) {
		    $bug{$_} = $value->[2];
		}
	    }

	    if ($key eq 'originator') {
		$bug{originator} = &$sub_parse_email (@$value);

		if (ref $bug{originator}) {
		    $bug{from} = $bug{originator}->[0];
		    $bug{email} = $bug{originator}->[1];
		} else {
		    $bug{email} = $bug{originator};
		}
	    }

	    foreach (qw[done forwarded]) {
		if ($key eq $_) {
		    $bug{$_} = &$sub_parse_email (@$value);
		}
	    }

	    if ($key eq 'message') {
		$bug{message} = &$sub_parse_message (@$value);
	    }

	    if ($key eq 'extras') {
		my @values = @$value;
	
		for (my $j=3 ;$j < $#values; $j=$j+4) {
		    my ($subkey, $subvalue) = ($value->[$j], $value->[$j+1]);

		    if ($subkey eq 'message') {
			my $message = &$sub_parse_message (@$subvalue);
			if (ref $bug{extra}) {
			    push @{$bug{extra}}, $message;
			} else {
			    $bug{extra} = [$message];
			}
		    }
		}
	    }
	}

	push @bugs, {%bug};
    }

    return [@bugs];
}


1;
